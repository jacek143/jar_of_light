#ifndef HUE_SWEEPER_H
#define HUE_SWEEPER_H

#include "RGB_LED.h"

class Hue_Sweeper {
public:
    Hue_Sweeper(RGB_LED* pLED);
    void begin();
    void increment_hue();
private:
    void update_led();
    void compute_new_color();
    
    RGB_LED* pLED;
    uint8_t G;
    uint8_t R;
    uint8_t B;
};

#endif /* HUE_SWEEPER_H */
