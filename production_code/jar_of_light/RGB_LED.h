#ifndef RGB_LED_H
#define RGB_LED_H

#include <stdint.h>

class RGB_LED {
public:
    virtual void begin() = 0;
    virtual void set(uint8_t R, uint8_t G, uint8_t B) = 0;

    virtual ~RGB_LED() {
    };
};

#endif /* RGB_LED_H */
