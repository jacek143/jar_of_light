#include "Hue_Sweeper.h"

Hue_Sweeper::Hue_Sweeper(RGB_LED* pLED) {
    this->pLED = pLED;
    R = 255;
    G = 0;
    B = 0;
}

void Hue_Sweeper::begin() {
    pLED->begin();
    update_led();
}

void Hue_Sweeper::increment_hue() {
    compute_new_color();
    update_led();
}

void Hue_Sweeper::compute_new_color() {
    if (R == 255 && G < 255 && B == 0)
        G++;
    else if (R > 0 && G == 255 && B == 0)
        R--;
    else if (R == 0 && G == 255 && B < 255)
        B++;
    else if (R == 0 && G > 0 && B == 255)
        G--;
    else if (R < 255 && G == 0 && B == 255)
        R++;
    else
        B--;
}

void Hue_Sweeper::update_led() {
    pLED->set(R, G, B);
}