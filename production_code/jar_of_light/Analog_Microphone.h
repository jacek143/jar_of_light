#ifndef ANALOG_MICROPHONE_H
#define ANALOG_MICROPHONE_H

#include "Analog_Sensor.h"

class Analog_Microphone : public Analog_Sensor {
  public:
    uint16_t read() {
      return analogRead(0);
    }
};

#endif /* ANALOG_MICROPHONE_H */
