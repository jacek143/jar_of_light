#ifndef STATISTICS_H
#define STATISTICS_H

#include <stdint.h>

class Statistics {
public:
    float standard_deviation(float samples[], uint8_t count);
    float average(float samples[], uint8_t count);
};


#endif /* STATISTICS_H */

