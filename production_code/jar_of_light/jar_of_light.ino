#include "NeoPixel_Wrapper.h"
#include "Hue_Sweeper.h"
#include "Analog_Microphone.h"
#include "Statistics.h"

#define DEVIATION_THRESHOLD 5

NeoPixel_Wrapper pixel = NeoPixel_Wrapper();
Hue_Sweeper sweeper = Hue_Sweeper(&pixel);
Analog_Microphone microphone = Analog_Microphone();
Statistics statistics = Statistics();

#define SAMPLES_COUNT 50
float samples[SAMPLES_COUNT] = {};

void setup() {
  sweeper.begin();
  Serial.begin(115200);
}

void loop() {
  for (int i = 0; i < SAMPLES_COUNT; i++)
    samples[i] = microphone.read();
  if (statistics.standard_deviation(samples, SAMPLES_COUNT) > DEVIATION_THRESHOLD)
    sweeper.increment_hue();
}
