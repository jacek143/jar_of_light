#include "NeoPixel_Wrapper.h"

#define NUMPIXELS      1

NeoPixel_Wrapper::NeoPixel_Wrapper(uint8_t pin) {
  pixels = Adafruit_NeoPixel(NUMPIXELS, pin, NEO_GRB + NEO_KHZ800);
}

void NeoPixel_Wrapper::begin() {
  pixels.begin();
}

void NeoPixel_Wrapper::set(uint8_t R, uint8_t G, uint8_t B) {
  uint8_t pixel_id = 0;
  pixels.setPixelColor(pixel_id, pixels.Color(R, G, B));
  pixels.show();
}
