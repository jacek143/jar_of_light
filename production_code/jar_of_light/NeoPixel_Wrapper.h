#ifndef NEOPIXEL_WRAPPER_H
#define NEOPIXEL_WRAPPER_H

#include "RGB_LED.h"
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif

class NeoPixel_Wrapper : public RGB_LED  {
  public:
    NeoPixel_Wrapper(uint8_t pin = 6);
    void begin();
    void set(uint8_t R, uint8_t G, uint8_t B);
  private:
    Adafruit_NeoPixel pixels;
};

#endif /* NEOPIXEL_WRAPPER_H */
