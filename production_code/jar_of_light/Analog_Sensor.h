#ifndef ANALOG_SENSOR_READER_H
#define ANALOG_SENSOR_READER_H

#include <stdint.h>

class Analog_Sensor {
public:
    virtual uint16_t read() = 0;

    virtual ~Analog_Sensor() {
    };
};

#endif /* RAW_ANALOG_READER_H */
