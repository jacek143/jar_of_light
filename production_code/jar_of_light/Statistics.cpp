#include <math.h>
#include "Statistics.h"

float Statistics::average(float samples[], uint8_t count) {
    if (!samples || count == 0)
        return NAN;
    float sum = 0;
    for (int i = 0; i < count; i++)
        sum += samples[i];
    return sum / count;
}

float Statistics::standard_deviation(float samples[], uint8_t count) {
    if (!samples || count == 0)
        return NAN;
    if (count == 1)
        return 0;

    float avr = average(samples, count);
    float sum_of_square_diff = 0;
    for (uint8_t i = 0; i < count; i++) {
        sum_of_square_diff += pow(samples[i] - avr, 2);
    }
    float variance = sum_of_square_diff / count;
    return sqrt(variance);
}