#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=MinGW-Windows
CND_DLIB_EXT=dll
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/gtest/src/gtest-all.o \
	${OBJECTDIR}/production_code/jar_of_light/Hue_Sweeper.o \
	${OBJECTDIR}/production_code/jar_of_light/Statistics.o

# Test Directory
TESTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}/tests

# Test Files
TESTFILES= \
	${TESTDIR}/TestFiles/f1

# Test Object Files
TESTOBJECTFILES= \
	${TESTDIR}/analog_sensor_fake_test.o \
	${TESTDIR}/average_test.o \
	${TESTDIR}/unit_tests/analog_sensor_fake.o \
	${TESTDIR}/unit_tests/hue_sweeper_test.o \
	${TESTDIR}/unit_tests/rgb_led_fake.o \
	${TESTDIR}/unit_tests/rgb_led_fake_test.o \
	${TESTDIR}/unit_tests/standard_deviation_test.o \
	${TESTDIR}/unit_tests/test_runner.o

# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libjar_of_light.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libjar_of_light.a: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libjar_of_light.a
	${AR} -rv ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libjar_of_light.a ${OBJECTFILES} 
	$(RANLIB) ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libjar_of_light.a

${OBJECTDIR}/gtest/src/gtest-all.o: gtest/src/gtest-all.cc
	${MKDIR} -p ${OBJECTDIR}/gtest/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -Igtest -Iproduction_code/jar_of_light -Iunit_tests -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/gtest/src/gtest-all.o gtest/src/gtest-all.cc

${OBJECTDIR}/production_code/jar_of_light/Hue_Sweeper.o: production_code/jar_of_light/Hue_Sweeper.cpp
	${MKDIR} -p ${OBJECTDIR}/production_code/jar_of_light
	${RM} "$@.d"
	$(COMPILE.cc) -g -Igtest -Iproduction_code/jar_of_light -Iunit_tests -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/production_code/jar_of_light/Hue_Sweeper.o production_code/jar_of_light/Hue_Sweeper.cpp

${OBJECTDIR}/production_code/jar_of_light/Statistics.o: production_code/jar_of_light/Statistics.cpp
	${MKDIR} -p ${OBJECTDIR}/production_code/jar_of_light
	${RM} "$@.d"
	$(COMPILE.cc) -g -Igtest -Iproduction_code/jar_of_light -Iunit_tests -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/production_code/jar_of_light/Statistics.o production_code/jar_of_light/Statistics.cpp

# Subprojects
.build-subprojects:

# Build Test Targets
.build-tests-conf: .build-tests-subprojects .build-conf ${TESTFILES}
.build-tests-subprojects:

${TESTDIR}/TestFiles/f1: ${TESTDIR}/unit_tests/analog_sensor_fake.o ${TESTDIR}/analog_sensor_fake_test.o ${TESTDIR}/average_test.o ${TESTDIR}/unit_tests/hue_sweeper_test.o ${TESTDIR}/unit_tests/rgb_led_fake.o ${TESTDIR}/unit_tests/rgb_led_fake_test.o ${TESTDIR}/unit_tests/standard_deviation_test.o ${TESTDIR}/unit_tests/test_runner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f1 $^ ${LDLIBSOPTIONS}   


${TESTDIR}/unit_tests/analog_sensor_fake.o: unit_tests/analog_sensor_fake.cpp 
	${MKDIR} -p ${TESTDIR}/unit_tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -Igtest -Iproduction_code/jar_of_light -Iunit_tests -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/unit_tests/analog_sensor_fake.o unit_tests/analog_sensor_fake.cpp


${TESTDIR}/analog_sensor_fake_test.o: analog_sensor_fake_test.cpp 
	${MKDIR} -p ${TESTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Igtest -Iproduction_code/jar_of_light -Iunit_tests -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/analog_sensor_fake_test.o analog_sensor_fake_test.cpp


${TESTDIR}/average_test.o: average_test.cpp 
	${MKDIR} -p ${TESTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Igtest -Iproduction_code/jar_of_light -Iunit_tests -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/average_test.o average_test.cpp


${TESTDIR}/unit_tests/hue_sweeper_test.o: unit_tests/hue_sweeper_test.cpp 
	${MKDIR} -p ${TESTDIR}/unit_tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -Igtest -Iproduction_code/jar_of_light -Iunit_tests -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/unit_tests/hue_sweeper_test.o unit_tests/hue_sweeper_test.cpp


${TESTDIR}/unit_tests/rgb_led_fake.o: unit_tests/rgb_led_fake.cpp 
	${MKDIR} -p ${TESTDIR}/unit_tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -Igtest -Iproduction_code/jar_of_light -Iunit_tests -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/unit_tests/rgb_led_fake.o unit_tests/rgb_led_fake.cpp


${TESTDIR}/unit_tests/rgb_led_fake_test.o: unit_tests/rgb_led_fake_test.cpp 
	${MKDIR} -p ${TESTDIR}/unit_tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -Igtest -Iproduction_code/jar_of_light -Iunit_tests -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/unit_tests/rgb_led_fake_test.o unit_tests/rgb_led_fake_test.cpp


${TESTDIR}/unit_tests/standard_deviation_test.o: unit_tests/standard_deviation_test.cpp 
	${MKDIR} -p ${TESTDIR}/unit_tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -Igtest -Iproduction_code/jar_of_light -Iunit_tests -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/unit_tests/standard_deviation_test.o unit_tests/standard_deviation_test.cpp


${TESTDIR}/unit_tests/test_runner.o: unit_tests/test_runner.cpp 
	${MKDIR} -p ${TESTDIR}/unit_tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -Igtest -Iproduction_code/jar_of_light -Iunit_tests -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/unit_tests/test_runner.o unit_tests/test_runner.cpp


${OBJECTDIR}/gtest/src/gtest-all_nomain.o: ${OBJECTDIR}/gtest/src/gtest-all.o gtest/src/gtest-all.cc 
	${MKDIR} -p ${OBJECTDIR}/gtest/src
	@NMOUTPUT=`${NM} ${OBJECTDIR}/gtest/src/gtest-all.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Igtest -Iproduction_code/jar_of_light -Iunit_tests -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/gtest/src/gtest-all_nomain.o gtest/src/gtest-all.cc;\
	else  \
	    ${CP} ${OBJECTDIR}/gtest/src/gtest-all.o ${OBJECTDIR}/gtest/src/gtest-all_nomain.o;\
	fi

${OBJECTDIR}/production_code/jar_of_light/Hue_Sweeper_nomain.o: ${OBJECTDIR}/production_code/jar_of_light/Hue_Sweeper.o production_code/jar_of_light/Hue_Sweeper.cpp 
	${MKDIR} -p ${OBJECTDIR}/production_code/jar_of_light
	@NMOUTPUT=`${NM} ${OBJECTDIR}/production_code/jar_of_light/Hue_Sweeper.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Igtest -Iproduction_code/jar_of_light -Iunit_tests -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/production_code/jar_of_light/Hue_Sweeper_nomain.o production_code/jar_of_light/Hue_Sweeper.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/production_code/jar_of_light/Hue_Sweeper.o ${OBJECTDIR}/production_code/jar_of_light/Hue_Sweeper_nomain.o;\
	fi

${OBJECTDIR}/production_code/jar_of_light/Statistics_nomain.o: ${OBJECTDIR}/production_code/jar_of_light/Statistics.o production_code/jar_of_light/Statistics.cpp 
	${MKDIR} -p ${OBJECTDIR}/production_code/jar_of_light
	@NMOUTPUT=`${NM} ${OBJECTDIR}/production_code/jar_of_light/Statistics.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Igtest -Iproduction_code/jar_of_light -Iunit_tests -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/production_code/jar_of_light/Statistics_nomain.o production_code/jar_of_light/Statistics.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/production_code/jar_of_light/Statistics.o ${OBJECTDIR}/production_code/jar_of_light/Statistics_nomain.o;\
	fi

# Run Test Targets
.test-conf:
	@if [ "${TEST}" = "" ]; \
	then  \
	    ${TESTDIR}/TestFiles/f1 || true; \
	else  \
	    ./${TEST} || true; \
	fi

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
