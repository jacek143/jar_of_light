#ifndef ANALOG_SENSOR_FAKE_H
#define ANALOG_SENSOR_FAKE_H

#include <cstdint>
#include "Analog_Sensor.h"

class Analog_Sensor_Fake : public Analog_Sensor {
public:
    Analog_Sensor_Fake();
    uint16_t read();

    uint16_t return_value;
};

#endif /* ANALOG_READ_FAKE_H */

