#include <gtest/gtest.h>
#include <cmath>
#include "Statistics.h"

class AverageTestSuite : public testing::Test {
protected:
    Statistics* p_statistics;

    void SetUp() {
        p_statistics = new Statistics();
    }

    void TearDown() {
        delete p_statistics;
        p_statistics = 0;
    }
};

TEST_F(AverageTestSuite, testNull) {
    ASSERT_TRUE(std::isnan(p_statistics->average(nullptr, 43)));
}

TEST_F(AverageTestSuite, testNoSamples) {
    float samples[] = {3.44, 3.3};
    ASSERT_TRUE(std::isnan(p_statistics->average(samples, 0)));
}

TEST_F(AverageTestSuite, testOneSample) {
    float samples[] = {122.3};
    ASSERT_FLOAT_EQ(122.3, p_statistics->average(samples, 1));
}

TEST_F(AverageTestSuite, testTwoSample) {
    float samples[] = {0.4, 0.2};
    ASSERT_FLOAT_EQ(0.3, p_statistics->average(samples, 2));
}

TEST_F(AverageTestSuite, testManySamples) {
    float samples[] = {0.4, 0.5, 0.9, 1.0};
    ASSERT_FLOAT_EQ(0.7, p_statistics->average(samples, 4));
}