#ifndef RGB_LED_FAKE_H
#define RGB_LED_FAKE_H

#include "RGB_LED.h"

class RGB_LED_Fake : public RGB_LED {
public:
    RGB_LED_Fake();
    void begin();
    void set(uint8_t R, uint8_t G, uint8_t B);
    virtual ~RGB_LED_Fake();

    uint8_t begin_count;
    uint8_t set_count;
    uint8_t last_R;
    uint8_t last_G;
    uint8_t last_B;
};

#endif /* RGB_LED_FAKE_H */

