#include "analog_sensor_fake.h"

Analog_Sensor_Fake::Analog_Sensor_Fake() {
    return_value = 0;
}

uint16_t Analog_Sensor_Fake::read() {
    return return_value;
}