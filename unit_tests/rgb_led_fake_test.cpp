#include <gtest/gtest.h>
#include "rgb_led_fake.h"

class RGB_LED_Fake_Test_Suite : public testing::Test {
protected:
};

TEST_F(RGB_LED_Fake_Test_Suite, construction) {
    RGB_LED_Fake spy;
    ASSERT_EQ(spy.begin_count, 0);
    ASSERT_EQ(spy.set_count, 0);
}

TEST_F(RGB_LED_Fake_Test_Suite, begin) {
    RGB_LED_Fake spy;
    ASSERT_EQ(spy.begin_count, 0);

    spy.begin();
    ASSERT_EQ(spy.begin_count, 1);

    spy.begin();
    spy.begin();
    ASSERT_EQ(spy.begin_count, 3);
}

TEST_F(RGB_LED_Fake_Test_Suite, set) {
    RGB_LED_Fake spy;
    ASSERT_EQ(spy.set_count, 0);

    spy.set(100, 200, 50);
    ASSERT_EQ(spy.set_count, 1);
    ASSERT_EQ(spy.last_R, 100);
    ASSERT_EQ(spy.last_G, 200);
    ASSERT_EQ(spy.last_B, 50);

    spy.set(1, 2, 3);
    spy.set(90, 170, 80);
    ASSERT_EQ(spy.set_count, 3);
    ASSERT_EQ(spy.last_R, 90);
    ASSERT_EQ(spy.last_G, 170);
    ASSERT_EQ(spy.last_B, 80);
}