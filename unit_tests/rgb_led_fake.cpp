#include "rgb_led_fake.h"

RGB_LED_Fake::RGB_LED_Fake() {
    begin_count = 0;
    set_count = 0;
}

RGB_LED_Fake::~RGB_LED_Fake() {
}

void RGB_LED_Fake::begin() {
    begin_count++;
}

void RGB_LED_Fake::set(uint8_t R, uint8_t G, uint8_t B) {
    set_count++;
    last_R = R;
    last_G = G;
    last_B = B;
}