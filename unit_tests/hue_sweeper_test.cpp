#include <gtest/gtest.h>

#include "RGB_LED.h"
#include "rgb_led_fake.h"
#include "Hue_Sweeper.h"

class HueSweeperTestSuite : public testing::Test {
protected:
    RGB_LED_Fake* pLED;
    Hue_Sweeper* pSweeper;

    enum Colors {
        RED = 0, YELLOW, GREEN, CYAN, BLUE, MAGENTA
    };

    void SetUp() {
        pLED = new RGB_LED_Fake();
        pSweeper = new Hue_Sweeper(pLED);
        pSweeper->begin();
    }

    void TearDown() {
        delete pLED;
        pLED = 0;
        delete pSweeper;
        pSweeper = 0;
    }

    void expect_color(uint8_t R, uint8_t G, uint8_t B) {
        ASSERT_EQ(pLED->last_R, R);
        ASSERT_EQ(pLED->last_G, G);
        ASSERT_EQ(pLED->last_B, B);
    }
};

TEST_F(HueSweeperTestSuite, begin) {
    ASSERT_EQ(pLED->begin_count, 1);
    expect_color(255, 0, 0);
}

TEST_F(HueSweeperTestSuite, increment_size) {
    pSweeper->increment_hue();
    expect_color(255, 1, 0);
}

TEST_F(HueSweeperTestSuite, increment_size_many) {
    uint16_t increments = 32;
    for (int i = 0; i < increments; i++) {
        pSweeper->increment_hue();
    }
    uint16_t red_diff = abs(pLED->last_R - 255);
    uint16_t green_diff = abs(pLED->last_G - 0);
    uint16_t blue_diff = abs(pLED->last_B - 0);
    uint16_t total_diff = red_diff + green_diff + blue_diff;
    ASSERT_EQ(total_diff, increments);
}

TEST_F(HueSweeperTestSuite, sweep_to_yellow) {
    for (int i = 0; i < YELLOW * 255; i++) {
        pSweeper->increment_hue();
    }
    expect_color(255, 255, 0);
}

TEST_F(HueSweeperTestSuite, sweep_to_green) {
    for (int i = 0; i < GREEN * 255; i++) {
        pSweeper->increment_hue();
    }
    expect_color(0, 255, 0);
}

TEST_F(HueSweeperTestSuite, sweep_to_cyan) {
    for (int i = 0; i < CYAN * 255; i++) {
        pSweeper->increment_hue();
    }
    expect_color(0, 255, 255);
}

TEST_F(HueSweeperTestSuite, sweep_to_blue) {
    for (int i = 0; i < BLUE * 255; i++) {
        pSweeper->increment_hue();
    }
    expect_color(0, 0, 255);
}

TEST_F(HueSweeperTestSuite, sweep_to_magenta) {
    for (int i = 0; i < MAGENTA * 255; i++) {
        pSweeper->increment_hue();
    }
    expect_color(255, 0, 255);
}

TEST_F(HueSweeperTestSuite, full_hue_circle) {
    for (int i = 0; i < (MAGENTA + 1) * 255; i++) {
        pSweeper->increment_hue();
    }
    expect_color(255, 0, 0);
}

TEST_F(HueSweeperTestSuite, over_circle_to_yellow) {
    for (int i = 0; i < (MAGENTA + 2) * 255; i++) {
        pSweeper->increment_hue();
    }
    expect_color(255, 255, 0);
}
