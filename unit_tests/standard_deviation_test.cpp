#include <gtest/gtest.h>
#include <cmath>
#include <complex>
#include "Statistics.h"

class StandardDeviationTestSuite : public testing::Test {
protected:
    Statistics* p_statistics;

    void SetUp() {
        p_statistics = new Statistics();
    }

    void TearDown() {
        delete p_statistics;
        p_statistics = 0;
    }
};

TEST_F(StandardDeviationTestSuite, testNoSamples) {
    float samples[] = {};
    ASSERT_TRUE(std::isnan(p_statistics->standard_deviation(samples, 0)));
}

TEST_F(StandardDeviationTestSuite, testNull) {
    ASSERT_TRUE(std::isnan(p_statistics->standard_deviation(nullptr, 34)));
}

TEST_F(StandardDeviationTestSuite, testOneSamples) {
    float samples[] = {34.34};
    ASSERT_FLOAT_EQ(0, p_statistics->standard_deviation(samples, 1));
}

TEST_F(StandardDeviationTestSuite, testEgalitarian) {
    float samples[] = {4, 4, 4, 4, 4, 4};
    ASSERT_FLOAT_EQ(0, p_statistics->standard_deviation(samples, 6));
}

TEST_F(StandardDeviationTestSuite, testTwoSamples) {
    float samples[] = {20, 80};
    ASSERT_FLOAT_EQ(30, p_statistics->standard_deviation(samples, 2));
}

TEST_F(StandardDeviationTestSuite, testManySamples) {
    float samples[] = {2, 5, 6.5, 8, 11};
    ASSERT_FLOAT_EQ(3.0, p_statistics->standard_deviation(samples, 5));
}