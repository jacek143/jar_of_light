#include <gtest/gtest.h>
#include "analog_sensor_fake.h"

class AnalogSensorFakeTestSuite : public testing::Test {
protected:
    Analog_Sensor_Fake* pFake;

    void SetUp() {
        pFake = new Analog_Sensor_Fake();
    }

    void TearDown() {
        delete pFake;
        pFake = 0;
    }
};

TEST_F(AnalogSensorFakeTestSuite, testConstruct) {
    ASSERT_EQ(pFake->read(), 0);
}

TEST_F(AnalogSensorFakeTestSuite, testSetReturn) {
    pFake->return_value = 103;
    ASSERT_EQ(pFake->read(), 103);
}